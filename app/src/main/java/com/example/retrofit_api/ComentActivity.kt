package com.example.retrofit_api

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.example.retrofit_api.Retrofit.RetrofitAPI
import com.example.retrofit_api.Retrofit.comentsItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val  uri = "https://jsonplaceholder.typicode.com/"
lateinit var userComent:TextView;
class ComentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coment)
        userComent = findViewById(R.id.coment)
        userComent.setOnClickListener {
            val inent = Intent(this, AllbumActivity::class.java)
            startActivity(inent)
            finish()
        }
        getComents();
    }


    private fun getComents() {
       val  comentRetrofit = Retrofit.Builder()
           .addConverterFactory(GsonConverterFactory.create())
           .baseUrl(uri)
           .build()
           .create(RetrofitAPI::class.java)
        var comentData = comentRetrofit.getComent()
        comentData.enqueue(object : Callback<List<comentsItem>?> {
            override fun onResponse(
                call: Call<List<comentsItem>?>,
                response: Response<List<comentsItem>?>

            ) {
             val comentString = StringBuffer()
              var comentRespone = response.body()!!

              for (coment in comentRespone){
               userComent.text =   comentString.append(coment.email)
                  comentString.append(coment.name)
                  comentString.append("\n")
              }

            }

            override fun onFailure(call: Call<List<comentsItem>?>, t: Throwable) {
                Log.d("ComentActivity", "failuar"+t.message)
            }
        })

    }
}