package com.example.retrofit_api

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.example.retrofit_api.Retrofit.RetrofitAPI
import com.example.retrofit_api.Retrofit.photosItem
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

var photosUri = "https://jsonplaceholder.typicode.com/"
lateinit var photo_id:TextView
class PhotosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)

        photo_id = findViewById(R.id.photo_text)
        photo_id.setOnClickListener {
            val intent = Intent(this, VideoActivity::class.java)
            startActivity(intent)
        }

        getPhotos()
    }

    private fun getPhotos() {
      var photosRetrofit = Retrofit.Builder()
          .addConverterFactory(GsonConverterFactory.create())
          .baseUrl(photosUri)
          .build()
          .create(RetrofitAPI::class.java)
        var photo_retofite = photosRetrofit.getPhotos()
        photo_retofite.enqueue(object : Callback<List<photosItem>?> {
            override fun onResponse(
                call: Call<List<photosItem>?>,
                response: Response<List<photosItem>?>
            ) {
                var photosRespone = response.body()!!
                var photoStringBuffer = StringBuffer()

                for (data in photosRespone){
                photo_id.text = photoStringBuffer.append(data.thumbnailUrl).toString()
                    photoStringBuffer.append("\n")
                }
            }

            override fun onFailure(call: Call<List<photosItem>?>, t: Throwable) {
                Log.d("PhotosActivity", "failuar"+t.message)
            }
        })

    }
}