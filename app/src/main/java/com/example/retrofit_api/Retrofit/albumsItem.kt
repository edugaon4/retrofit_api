package com.example.retrofit_api.Retrofit

data class albumsItem(
    val id: Int,
    val title: String,
    val userId: Int
)