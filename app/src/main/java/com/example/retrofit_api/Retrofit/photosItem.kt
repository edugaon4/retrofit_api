package com.example.retrofit_api.Retrofit

data class photosItem(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)