package com.example.retrofit_api.Retrofit

data class comentsItem(
    val body: String,
    val email: String,
    val id: Int,
    val name: String,
    val postId: Int
)