package com.example.retrofit_api.Retrofit

data class postItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)