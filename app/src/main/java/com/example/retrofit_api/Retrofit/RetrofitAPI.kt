package com.example.retrofit_api.Retrofit

import android.telecom.Call
import retrofit2.http.GET

interface RetrofitAPI {


    @GET("posts")
    fun getCourse():retrofit2.Call<List<postItem>>

    @GET("comments")
    fun  getComent(): retrofit2.Call<List<comentsItem>>

    @GET("albums")
    fun getAllbums(): retrofit2.Call<List<albumsItem>>

    @GET("photos")
    fun getPhotos(): retrofit2.Call<List<photosItem>>
}