package com.example.retrofit_api.Retrofit

data class UserModelItem(
    val dataLimit: String,
    val deviceNo: String,
    val duePayment: String,
    val expiryDate: String,
    val installedBy: String,
    val installedDate: String,
    val mobileNumber: String,
    val simNo: String,
    val speedLimit: String,
    val trackerType: String,
    val userName: String,
    val vehicle: String,
    val vehicleNo: String,
    val vid: Int
)