package com.example.retrofit_api

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import com.example.retrofit_api.Retrofit.RetrofitAPI
import com.example.retrofit_api.Retrofit.albumsItem
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

val  albumsUri = "https://jsonplaceholder.typicode.com/"
lateinit var albumsTextView: TextView
lateinit var nextage: LinearLayout
class AllbumActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_allbum)
        albumsTextView = findViewById(R.id.albums)
        nextage = findViewById(R.id.photo_page)
        nextage.setOnClickListener {
            val intent = Intent(this, PhotosActivity::class.java)
            startActivity(intent)
            finish()
        }
        getAlbums()
    }

    private fun getAlbums() {
        var albumsData = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(albumsUri)
            .build()
            .create(RetrofitAPI::class.java)
        var dataAlbums = albumsData.getAllbums()
        dataAlbums.enqueue(object : Callback<List<albumsItem>?> {
            override fun onResponse(
                call: Call<List<albumsItem>?>,
                response: Response<List<albumsItem>?>
            ) {
                var albumsRespone = response.body()!!
                var data = StringBuffer()

                for (albumsData in albumsRespone){
                    albumsTextView.text =   data.append(albumsData.id)
                    data.append("\n")

                }
            }

            override fun onFailure(call: Call<List<albumsItem>?>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}