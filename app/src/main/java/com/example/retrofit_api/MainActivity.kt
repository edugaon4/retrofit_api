package com.example.retrofit_api

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.retrofit_api.Retrofit.RetrofitAPI
import com.example.retrofit_api.Retrofit.UserModelItem
import com.example.retrofit_api.Retrofit.photosItem
import com.example.retrofit_api.Retrofit.postItem
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

const val  Base_Url = "https://jsonplaceholder.typicode.com/"
@SuppressLint("StaticFieldLeak")
private lateinit var username:TextView;
private lateinit var userid:TextView;
lateinit var Listpage:Button;
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getAllData()
        username = findViewById(R.id.user_name)
        Listpage = findViewById(R.id.list_page)
        Listpage.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle("AlertDialog")
            builder.setMessage("Delete you file from this computer")
            builder.setIcon(R.drawable.ic_baseline_error_24)
            builder.setPositiveButton("yes"){posetive, position ->
                Toast.makeText(this, "clicked yes", Toast.LENGTH_SHORT).show()
            }
            builder.setNeutralButton("Cancle"){negative, position ->
                Toast.makeText(this, "Clicked cancle", Toast.LENGTH_SHORT).show()
            }
            builder.setNegativeButton("No"){
                negative, position ->
                Toast.makeText(this, "Cliked no", Toast.LENGTH_SHORT).show()
            }
            var atertDaileg = builder.create()
            atertDaileg.setCancelable(false)
            atertDaileg.show()
//            val intent = Intent(this, ListActivity::class.java)
//            startActivityctivity(intent)
        }
        username.setOnClickListener {
            val intent = Intent(this, ComentActivity::class.java)
            startActivity(intent)
        }
        userid = findViewById(R.id.user_id)
        getUserData();
        getDetails();
    }

    private fun getAllData() {
       var retrofit = Retrofit.Builder()
           .addConverterFactory(GsonConverterFactory.create())
           .baseUrl("")
           .build()
           .create(RetrofitAPI::class.java)
        var response = retrofit.getCourse()
        response.enqueue(object : Callback<List<postItem>?> {
            override fun onResponse(
                call: Call<List<postItem>?>,
                response: Response<List<postItem>?>
            ) {
               var response1 = response.body()
                var baseUri = StringBuffer()
                if (response1 != null) {
                    for (data in response1){
                        baseUri.append(data.id)
                    }
                }
            }

            override fun onFailure(call: Call<List<postItem>?>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun getDetails() {
        var retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("")
            .build()
            .create(RetrofitAPI::class.java)
        val retrofile = retrofit.getCourse()
        retrofile.enqueue(object : Callback<List<postItem>?> {
            override fun onResponse(
                call: Call<List<postItem>?>,
                response: Response<List<postItem>?>
            ) {
               var responseData = response.body()
                val r = StringBuffer()
                if (responseData != null) {
                    for (a in responseData){
                        r.append("\n")
                        r.append(a.id)
                    }
                }
            }

            override fun onFailure(call: Call<List<postItem>?>, t: Throwable) {
                Log.d("MainActivity", "failluar",)
            }
        })
    }

    private fun getUserData() {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Base_Url)
            .build()
            .create(RetrofitAPI::class.java)
        var retrofiteData = retrofit.getCourse()

        retrofiteData.enqueue(object : Callback<List<postItem>?> {
            override fun onResponse(
                call: Call<List<postItem>?>,
                response: Response<List<postItem>?>
            ) {
                var responseDate = response.body()!!
                var stringBuffer = StringBuffer()

                for (myData in responseDate){
                    stringBuffer.append("\n")
                    username.text= stringBuffer.append(myData.id)
                    userid.text = stringBuffer.append(myData.title)

                }
            }

            override fun onFailure(call: Call<List<postItem>?>, t: Throwable) {
               // TODO("Not yet implemented")
                Log.d("MainActivity", "Failluar"+t.message,)
            }
        })
    }

}